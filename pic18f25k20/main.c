#include <main.h>
#include "dui.c"

unsigned int16 cVol = 0;
unsigned int16 cTimeStart = 0;
unsigned int8 cState = 0;

//TASK MESSAGE CHECK
#task(rate = 100 ms, max = 30 us)
void task_check();

#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

//-------------------------------------------------
void task_check() {
/*   unsigned int8 t = 0;
   output_float(PWM);
   while (input_state(PWM) == 0 && t < 200) t++;
   DUISendUInt8(0, t);
   output_low(PWM);*/
   switch (cState) {
   case 0: 
      cVol = get_capture(1) - cTimeStart;
      DUISendUInt16(0, cVol);
      DUISendStateTrueFalse(1, (cVol > 100));
      output_low(PWM);
      cState = 1;
      break;
   case 1: 
      
      cTimeStart = get_timer1();
      output_float(PWM);
      cState = 0;
      break;
   }

}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   char text1[]="Capacity";
   char text2[]="";
   DUIAddChart(0, text1, text2);
   char text3[]="Alarm";
   DUIAddLed(1, 0, text3);
}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
    output_low(PWM);
    setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);      //13,1 ms overflow
    setup_ccp1(CCP_CAPTURE_RE);
}

void main()
{
   initialization();
   rtos_run();
}
